package com.arreglos;

import java.util.Scanner;

public class Main {
    private static final String[] arreglo = new String[6];

    public static void main(String[] args) {
        llenar();
        mostrarInvertido();
    }

    private static void llenar() {
        Scanner entrada = new Scanner(System.in);
        for (int indice = 0; indice < arreglo.length; indice++) {
            System.out.println("Ingrese el caracter " + (indice + 1) + ":");
            arreglo[indice] = entrada.nextLine();
        }
        entrada.close();
    }

    private static void mostrarInvertido() {
        System.out.println("El arreglo invertido es: ");
        for (int indice = (arreglo.length - 1); indice >= 0; indice--) {
            System.out.println(arreglo[indice]);
        }
    }
}